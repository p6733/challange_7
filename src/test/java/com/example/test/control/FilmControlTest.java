package com.example.test.control;

import com.example.test.service.InvoiceServiceImpl;
import com.example.test.service.ReportServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;


@SpringBootTest
class FilmControlTest {
    @Autowired
    FilmControl filmControl;
    @Autowired
    ScheduleControl scheduleControl;
    @Autowired
    UserControl userControl;
    @Autowired
    SeatControl seatControl;
    @Autowired
    InvoiceControl invoiceControl;
//film
    @Test
    void addFilm() {
//        filmControl.addFilm("kukira rumah","tayang");
    }
    @Test
    void updateFilm(){//filmControl.updateFilm(2,"ruang rindu","tidak tayang");
        }
    @Test
    void finByStatus(){
        filmControl.showFilmByStatus("tayang");
    }
    @Test
    void deleteByName(){
        filmControl.deleteFilmByFilmName(10);
    }
    @Test
    void tetst(){
        filmControl.showFilmByFilmName("kukira rumah");
    }

  //schedule
    @Test
    void addSchedule() {
     //   scheduleControl.addSchedule("27 maret 2022",20000,"12:30","15:15",1);
    }
    @Test
    void  ShowscheduleByFilmName(){
        scheduleControl.showScheduleByFilmName("kukira rumah");
    }




    //user
    @Test
    void addUser(){
//        userControl.addUser("Dwii","iloveself","d@gmail.com");
    }
    @Test
    void updateUser(){
//        userControl.updateUser(2,"dwiku","qwerty","D@gmail  ");
    }
    @Test
    void findbyId(){
    //    userControl.findUserById(1);
    }
    @Test
    void deletebyId(){
    //    userControl.deleteUserById(6);
    }


    @Test
    void findScheduleByFilmName(){
        scheduleControl.showScheduleByFilmName("kukira rumah");
    }
    @Test
    void showScheduleByFilmName(){
        scheduleControl.showScheduleByFilmName("kukira rumah");
    }

    //seatt
    @Test
    void addSeat(){
       // seatControl.addSeat("A","12",1,1);
    }


    //Invoice
    @Test
    void invoice(){
        invoiceControl.invoice("dwii");
    }
    @Test
    void showbyIduser(){
        userControl.showUserById(1);
    }

    @Autowired
    ReportServiceImpl reportService;
    @Test
    void pdf(){
        reportService.generateReport(LocalDate.now(),"pdf");
    }
    @Autowired
    InvoiceServiceImpl invoiceService;
    @Test
    void pdfInvoice(){
        invoiceService.generateReport(LocalDate.now(),"pdf");
    }
}