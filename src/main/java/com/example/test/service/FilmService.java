package com.example.test.service;

import com.example.test.model.Film;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FilmService {
    void addFilm(String filmName, String statusTayang);
    List<Film> showFilm();
    Film getFilmById(Integer filmCode);
    void updateFilm(Integer filmCode,String filmName,String statusTayang);
    void deleteFilm(Integer filmCode);
    public List<Film> getByStatusTayang(String statusTayang);
    public  List<Film> getByFilmName(String filmName);
}
