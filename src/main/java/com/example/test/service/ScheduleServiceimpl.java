package com.example.test.service;

import com.example.test.model.Schedule;
import com.example.test.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class ScheduleServiceimpl implements ScheduleService {
    @PersistenceContext
    private EntityManager em;
    @Autowired
    ScheduleRepository scheduleRepository;
    @Autowired
    FilmService filmService;

    @Override
    public void addSchedule(String tanggalTayang,Integer hargaTiket, String jamMulai, String jamSelesai, Integer filmCode) {
        Schedule schedule=new Schedule();
        schedule.setTanggalTayang(tanggalTayang);
        schedule.setHargaTiket(hargaTiket);
        schedule.setJamMulai(jamMulai);
        schedule.setJamSelesai(jamSelesai);
        schedule.setFilmCode(filmService.getFilmById(filmCode));
        scheduleRepository.save(schedule);

    }

    //keperluan Foreign key
    @Override
    public Schedule findById(Integer scheduleId) {
        return scheduleRepository.getById(scheduleId);
    }

    @Override
    public List<Schedule> findScheduleByFilmName(String filmName) {
        return scheduleRepository.findByFilmName(filmName);
    }

    @Override
    public List<Schedule> showSchedule() {
        return scheduleRepository.findAll();
    }
}
