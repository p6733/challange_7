package com.example.test.service;

import com.example.test.model.User;
import com.example.test.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class UserServiceImpl implements UserService{
    @PersistenceContext
    private EntityManager em;
    @Autowired
    UserRepository userRepository;

    @Override
    public void addUser(String username, String password, String email) {
        User user=new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        userRepository.save(user);
    }
    //keperluan Foreign key
    @Override
    public User findUserById(Integer userId) {
        return userRepository.getById(userId);

    }

    @Override
    public List<User> getUserById(Integer userId) {
       return userRepository.findByUserId(userId);

    }

    @Override
    public void deleteUserById(Integer userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public void updateUser(Integer userId, String username, String password, String email) {
        userRepository.updateUser(username,password,email,userId);
    }

    @Override
    public List<User> userList() {
        return userRepository.findAll();
    }
}
