package com.example.test.service;

import com.example.test.model.Schedule;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ScheduleService {
    void addSchedule(String tanggalTayang,Integer hargaTiket, String jamMulai, String jamSelesai, Integer filmCode);
    Schedule findById(Integer scheduleId);
    List<Schedule> findScheduleByFilmName(String filmName);
    List<Schedule> showSchedule();
}
