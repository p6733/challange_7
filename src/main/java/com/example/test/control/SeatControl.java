package com.example.test.control;

import com.example.test.model.Seat;
import com.example.test.service.SeatService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "seat", description = "control pada seat")
@RestController
@RequestMapping("/seat")
public class SeatControl {
    @Autowired
    SeatService seatService;
    @PostMapping("/add-seat")
    void addSeat(@RequestBody Seat seat){
        seatService.addSeat(seat.getStudioName(),seat.getNomorKursi(),seat.getUserId().getUserId(),seat.getSchedulesId().getScheduleId());
    }
    @GetMapping("/show-seat")
    List<Seat> showSeat(){
        List<Seat>seat=seatService.showSeat();
        return seat;
    }
    @DeleteMapping("/delete-seat/{studio},{kursi}")
    void deleteSeat(@PathVariable("studio")String studioName,
                    @PathVariable("kursi")String nomorKursi){
        seatService.deleteSeat(studioName,nomorKursi);

    }
}
