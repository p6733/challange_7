package com.example.test.control;

import com.example.test.model.Schedule;
import com.example.test.service.ScheduleService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Tag(name = "schedule", description = "control pada schedule")
@RestController
@RequestMapping("/schedule")
public class ScheduleControl {
    @Autowired
    ScheduleService scheduleService;


    @PostMapping("/add-schedule")
    void addSchedule(@RequestBody Schedule schedule){
        scheduleService.addSchedule(schedule.getTanggalTayang(),schedule.getHargaTiket(),schedule.getJamMulai(),schedule.getJamSelesai(),schedule.getFilmCode().getFilmCode());
    }
    @GetMapping("/show-schedule-by-film/{filmName}")
    List<Schedule> showScheduleByFilmName(@PathVariable("filmName") String filmName){
        List<Schedule>schedule=scheduleService.findScheduleByFilmName(filmName);
        schedule.forEach(Schedule->{
            System.out.println(Schedule.getScheduleId()+"\t\t\t"+Schedule.getHargaTiket()+"\t\t\t"+Schedule.getTanggalTayang()+"\t\t\t"+Schedule.getJamMulai()
                    +"\t\t\t"+Schedule.getJamSelesai()+"\t\t\t"+Schedule.getFilmCode().getFilmCode()+"\t\t\t"+Schedule.getFilmCode().getFilmName());

        });
        return schedule;
    }
    @GetMapping("/show-schedule")
    List<Schedule> showSchedule(){
        List<Schedule>schedule=scheduleService.showSchedule();
        return schedule;
    }
}
