package com.example.test.control;

import com.example.test.model.Seat;
import com.example.test.service.SeatService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "invoice", description = "control pada invoice")
@RestController
@RequestMapping("/invoice")
public class
InvoiceControl {
    @Autowired
    SeatService seatService;

    @GetMapping("/invoice/{name}")
    List<Seat> invoice(@PathVariable ("name") String username){
        List<Seat> seat=seatService.invoice(username);
        seat.forEach(Seat->{
            System.out.println(Seat.getStudioName()+"\t\t\t"+Seat.getNomorKursi()+"\t\t\t"+Seat.getSchedulesId().getTanggalTayang()
                    +"\t\t\t"+Seat.getSchedulesId().getJamMulai()+"\t\t\t"+Seat.getSchedulesId().getJamSelesai()+"\t\t\t"+
                    Seat.getSchedulesId().getHargaTiket()+"\t\t\t"+Seat.getSchedulesId().getFilmCode().getFilmName()+"\t\t\t"+Seat.getUserId().getUsername()
            );
        });
        return seat;
    }
}
