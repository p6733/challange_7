package com.example.test.control;

import com.example.test.model.User;
import com.example.test.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Tag(name = "user", description = "control pada user")
@RestController
@RequestMapping("/user")
public class UserControl {
    @Autowired
    UserService userService;
    @Operation(summary = "add user to user entity")
    @PostMapping("/add-user")
    String addUser(@RequestBody User user){
        userService.addUser(user.getUsername(),user.getPassword(),user.getEmail());
        return "add user berhasil";
    }
    @GetMapping("/find-user-by-id/{id}")
    void findUserById(@PathVariable("id")Integer userId){
       userService.getUserById(userId);

    }

    @GetMapping("/show-user")
    public List<User>userList(){
        List<User>userList=userService.userList();
        return userList;
    }
    @DeleteMapping("/delete-user/{id}")
    String deleteUserById(@PathVariable("id") Integer userId){
        userService.deleteUserById(userId);
        return"delete user berhasil";
    }
    @PutMapping("/upadate-user")
    void updateUser(@RequestBody User user){
        userService.updateUser(user.getUserId(),user.getUsername(),user.getPassword(),user.getEmail());
    }
    @GetMapping("/show-user-by-id/{id}")
    List<User> showUserById(@PathVariable("id") Integer userId){
        List<User>user=userService.getUserById(userId);
        return user;
    }
}
