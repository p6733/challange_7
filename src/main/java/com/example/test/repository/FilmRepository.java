package com.example.test.repository;

import com.example.test.model.Film;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface FilmRepository extends JpaRepository<Film,Integer> {
    @Modifying
    @Query(value = "update film f set  f.film_name= :film_name, f.status_tayang= :status_tayang" +
            " where f.film_code= :film_code",nativeQuery = true)
    void updateFilm(@Param("film_code") Integer filmCode,
                    @Param("film_name") String filmName,
                    @Param("status_tayang") String statusTayang
    );
    void deleteFilmsByFilmCode(Integer filmCode);
     public List<Film> findByStatusTayang(String statusTayang);
     public List<Film> findByFilmName(String filmName);
}
