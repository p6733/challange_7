package com.example.test.repository;

import com.example.test.model.Film;
import com.example.test.model.Seat;
import com.example.test.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface SeatRepository extends JpaRepository<Seat,String> {
    @Modifying
    @Query(value = "select us.user_id, us.username, se.nomor_kursi, se.studio_name, se. user_id,se.schedule_id, sc.tanggal_tayang, " +
            " sc.jam_mulai, sc.jam_selesai,sc.harga_tiket, sc.film_code, fl.film_name from user us join seat se on se.user_id=us.user_id  " +
            "join schedule sc on sc.schedule_id = se.schedule_id join film fl on fl.film_code=sc.film_code where us.username = :username",nativeQuery = true)
    public List<Seat>ShowInvoice(@Param("username")String username);


    void deleteByStudioNameAndNomorKursi(@Param("studio_name")String studioName,
                                         @Param("nomor_kursi")String nomorKursi);
    List<Seat> findSeatByStudioNameAndNomorKursi(@Param("studio_name")String studioName,
                                              @Param("nomor_kursi")String nomorKurs);
}
