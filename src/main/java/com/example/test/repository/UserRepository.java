package com.example.test.repository;

import com.example.test.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User,Integer> {
    @Modifying
    @Query(value = "update user u set u.username= :username,u.password= :password,u.email= :email" +
            " where u.user_id= :user_id",nativeQuery = true)

    void updateUser(
            @Param("username") String username,
            @Param("password") String password,
            @Param("email") String email,
            @Param("user_id") Integer userId
    );

    @Override
    void deleteById(Integer userId);
    List<User> findByUserId(@Param("user_id")Integer userId);
    public User findByUsername(String username);
}
