package com.example.test.repository;
import com.example.test.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {
    @Modifying
    @Query(value = "select sc.schedule_id, sc.tanggal_tayang,sc.harga_tiket,sc.jam_mulai," +
            " sc.jam_selesai,sc.film_code, f.film_name from film f " +
            "  join schedule sc on  sc.film_code =f.film_code" +
            " where f.film_name= :film_name",nativeQuery = true)
    public List<Schedule> findByFilmName(@Param("film_name") String filmName);
}