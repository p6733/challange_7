package com.example.test.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
@Setter
@Getter
//@Embeddable
public class SeatId implements Serializable {
    @Id
    @Column(name ="studio_name" )
    private String studioName;

    @Id
    @Column(name ="nomor_kursi" )
    private String nomorKursi;


}
